<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionRequest;
use App\Services\TransactionService;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * @return [type]
     */
    public function store(TransactionRequest $request)
    {

        try {
            $transaction = (new TransactionService())->handle($request);

            return response()->json(['data' => $transaction], 201);
        } catch (\Throwable $th) {
            DB::rollBack();

            return $th->getMessage();
        }
    }
}
