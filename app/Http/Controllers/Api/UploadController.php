<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UploadFileRequest;

class UploadController extends Controller
{
    public function upload(UploadFileRequest $request)
    {

        try {
            $file = $request->file('file')->store('transactions');

            return response()->json([
                'data' => ['file_name' => ($file)],
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th->getMessage(),
            ], 400);
        }
    }
}
