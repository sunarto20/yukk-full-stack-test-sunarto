<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\HistoryService;

class HistoryController extends Controller
{
    public function index()
    {
        $histories = (new HistoryService())->handle();

        return response()->json($histories, 200);
    }
}
