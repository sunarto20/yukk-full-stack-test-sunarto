<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function getCurrentUser()
    {
        $userId = auth()->id();
        $user = User::query()->with('wallet')->where('id', $userId)->first();

        return response()->json($user, 200);
    }
}
