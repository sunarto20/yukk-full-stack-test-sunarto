<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Models\Wallet;
use App\Services\AuthServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $user = (new AuthServices())->handle($request);

            return response()->json(['data' => [
                'user' => $user['user'],
                'token' => $user['token'],
            ]], 200);
        } catch (\Exception $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        try {
            DB::beginTransaction();

            $data['password'] = bcrypt($request->password);

            $user = User::query()->create($data);

            Wallet::query()->create([
                'user_id' => $user->id,
                'virtual_account' => rand(1111111111, 9999999999),
            ]);

            DB::commit();

            return response()->json(['data' => $user->load('wallet')], Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json(['message' => $th->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
