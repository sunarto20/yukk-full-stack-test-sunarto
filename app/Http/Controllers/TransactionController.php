<?php

namespace App\Http\Controllers;

use App\Models\User;

class TransactionController extends Controller
{
    public function index()
    {
        $receivers = User::query()->whereNot('id', auth()->id())->get();

        return view('pages.transaction.index', compact('receivers'));
    }
}
