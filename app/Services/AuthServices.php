<?php

namespace App\Services;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthServices
{
    public function handle(LoginRequest $request): array
    {
        $user = User::query()->where('username', $request->username)->orWhere('email', $request->username)->first();

        if (! $user || ! password_verify($request->password, $user->password)) {
            throw new \Exception('Username or password not found', 400);
        }

        Auth::guard('web')->login($user);

        $token = auth('api')->login($user);

        return [
            'user' => $user,
            'token' => $token,
        ];
    }
}
