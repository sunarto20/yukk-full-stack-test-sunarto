<?php

namespace App\Services;

use App\Http\Requests\TransactionRequest;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    public function handle(TransactionRequest $request): Transaction
    {
        $userId = auth()->id();

        DB::beginTransaction();
        $wallet = Wallet::query()->where('user_id', $userId)->first();

        if ($request->type === 'top-up') {
            $amount = $wallet->amount + $request->amount;
        } elseif ($request->type === 'transaksi') {
            $amount = $wallet->amount - $request->amount;
            $receiver = $request->type === 'transaksi' ? $request->receiver : null;

            $receiverWallet = Wallet::query()->where('user_id', $receiver)->first();

            $receiverWallet->update(['amount' => $request->amount + $receiverWallet->amount]);
        }

        $wallet->update(['amount' => $amount]);

        $transaction = Transaction::create([
            'sender' => $userId,
            'receiver' => $receiver ?? null,
            'amount' => $request->amount,
            'note' => $request->note,
            'type' => $request->type,
            'code' => fake()->regexify('[A-Z]{5}[0-4]{3}'),
            'file' => $request->file ?? null,
        ]);

        DB::commit();

        return $transaction;
    }
}
