<?php

namespace App\Services;

use App\Models\Transaction;

class HistoryService
{
    public function handle()
    {
        $userId = auth()->id();

        $search = request('search') ?? null;
        $page = request('page') ?? 1;
        $filter = request('filter') ?? null;

        $transactions = Transaction::query()
            ->where('sender', '=', $userId)
            ->when($search, function ($query) use ($search) {
                $query->where('code', 'like', "%$search%")->orWhere('note', 'like', "%$search%");
            })
            ->when($filter, function ($query) use ($filter) {
                $query->where('type', 'like', "%$filter%");
            })->orderByDesc('created_at')
            ->paginate(1, '*', 'page', $page);

        return $transactions;
    }
}
