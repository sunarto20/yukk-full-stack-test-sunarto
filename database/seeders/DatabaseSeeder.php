<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $user1 = User::create([
            'name' => 'User 1',
            'email' => 'user1@tes.com',
            'password' => bcrypt('password'),
            'username' => 'user1',
        ]);

        Wallet::create([
            'user_id' => $user1->id,
            'virtual_account' => rand(1111111111, 9999999999),
            'amount' => 0,
        ]);
        $user2 = User::create([
            'name' => 'User 2',
            'email' => 'user2@tes.com',
            'password' => bcrypt('password'),
            'username' => 'user2',
        ]);

        Wallet::create([
            'user_id' => $user2->id,
            'virtual_account' => rand(1111111111, 9999999999),
            'amount' => 0,
        ]);
    }
}
