<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->unsignedBigInteger('sender');
            $table->unsignedBigInteger('receiver')->nullable();
            $table->string('type', 20);
            $table->double('amount');
            $table->longText('note')->nullable();
            $table->string('file')->nullable();
            $table->foreign('sender')->references('id')->on('users')->noActionOnDelete();
            $table->foreign('receiver')->references('id')->on('users')->noActionOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
