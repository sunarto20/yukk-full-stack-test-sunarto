const buttonSubmit = $("#btn-submit");

const fields = {
    username: $("#username"),
    password: $("#password"),
};

const alertError = $(".alert");

const login = () => {
    alertError.hide();
    $.ajax({
        url: `${BASE_URL_API}/login`,
        method: "POST",
        dataType: "JSON",
        data: {
            username: fields.username.val(),
            password: fields.password.val(),
        },
        success: (res) => handleResponse(res),
        error: (e) => handleError(e),
    });
};

buttonSubmit.on("click", function (e) {
    e.preventDefault();

    login();
});

const handleError = (error) => {
    const { errors, message } = error.responseJSON;
    console.log(error);

    if (error.status == 400) {
        alertError.show().text(message);
    } else {
        for (const field in fields) {
            if (errors.hasOwnProperty(field)) {
                fields[field]
                    .addClass("is-invalid")
                    .next()
                    .text(errors[field][0]);
            } else {
                fields[field].removeClass("is-invalid");
            }
        }
    }
};

const handleResponse = (response) => {
    const { token } = response.data;
    window.localStorage.setItem("token", token);
    setCookie("token", token);

    window.location.href = "/";
};

$(function () {
    alertError.hide();
});
