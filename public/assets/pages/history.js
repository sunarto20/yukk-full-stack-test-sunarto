const USER_API = `${BASE_URL}/api`;

const searchButton = $("#search-button");
const filter = $("#filter");
const resetFilter = $("#reset-filter");

searchButton.on("click", function () {
    setParams({ ...getParams(), search: $("#search").val(), page: 1 });
    getHistories();
});

filter.on("change", function () {
    setParams({ ...getParams(), filter: $("#filter").val(), page: 1 });
    getHistories();
});

resetFilter.on("click", function () {
    setParams({ ...getParams(), filter: null, page: 1 });
    filter.val("");
    getHistories();
});

$(document).on("click", "#loadMore", function () {
    const { page } = getParams();
    setParams({ ...getParams(), page: page + 1 });
    $.ajax({
        url: `${USER_API}/histories`,
        method: "GET",
        dataType: "JSON",
        data: getParams(),
        success: (res) => {
            let content = "";

            res.data.forEach((data) => {
                content += templateHistoryView(data);
            });

            if (res.current_page < res.last_page) {
                $("#loadMore").show();
            } else {
                $("#loadMore").hide();
            }

            $("#historyCard").append(content);
        },
        error: (err) => console.log(err),
    });
});

const getUser = () => {
    $.ajax({
        url: `${USER_API}/user`,
        method: "GET",
        dataType: "JSON",
        success: (res) => {
            setUserView(res);
        },
        error: (err) => console.log(err),
    });
};

const getHistories = () => {
    $.ajax({
        url: `${USER_API}/histories`,
        method: "GET",
        dataType: "JSON",
        data: getParams(),
        success: (res) => {
            let content = "";

            $("#historyCard").html("");
            res.data.forEach((data) => {
                content += templateHistoryView(data);
            });

            if (res.current_page < res.last_page) {
                $("#loadMore").show();
            } else {
                $("#loadMore").hide();
            }

            $("#historyCard").append(content);
        },
        error: (err) => console.log(err),
    });
};

const setUserView = (res) => {
    $("#userName").text(res.name);
    $("#virtualAccount").text(res.wallet.virtual_account);
    $("#userEmail").text(res.email);
    $("#balance").text(res.wallet.amount.toLocaleString("id-ID"));
};

const templateHistoryView = (data) => {
    const type =
        data.type === "transaksi"
            ? '<span class="badge-primary badge">Transaksi</span>'
            : '<span class="badge-success badge">Top Up</span>';

    return `<div class="row my-4">
                <div class="col-12 bg-light py-2 px-4 rounded d-flex justify-content-between ">
                    <div class="">
                        <div class="d-flex align-items-start align-content-center">
                            <p class='mr-2'>${data.code}</p>
                            ${type}
                        </div>
                        <div class="mt-2">
                            <p class="mb-0">Amount</p>
                            <p class="font-weight-bold">Rp. ${data.amount.toLocaleString(
                                "id-ID"
                            )}</p>
                            <span class="text-secondary font-italic">Note: ${
                                data.note
                            }</span>
                        </div>
                    </div>
                    <div class="text-right">
                        <div class="text-secondary">
                            <p class="mb-0 ">Created</p>
                            <p>${convertDate(data.created_at)}</p>
                        </div>
                    </div>
                </div>
            </div>`;
};

const convertDate = (date) => {
    date = new Date(date);
    return timeHandler(date);
};

const timeHandler = (date) => {
    let year = date.getFullYear(),
        month = date.toLocaleString("id-ID", { month: "long" }),
        day = parseTwoDigit(date.getDate()),
        hours = parseTwoDigit(date.getHours()),
        minutes = parseTwoDigit(date.getMinutes()),
        seconds = parseTwoDigit(date.getSeconds());

    return `${day} ${month} ${year} ${hours}:${minutes}:${seconds}`;
};

const parseTwoDigit = (time) => {
    return ("0" + time).slice(-2);
};

function getParams() {
    return JSON.parse(window.localStorage.getItem("param"));
}

function setParams(param) {
    window.localStorage.setItem("param", JSON.stringify(param));
}

$(function () {
    setParams({ page: 1, filter: null, search: null });
    getUser();
    getHistories();
});
