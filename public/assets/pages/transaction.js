const BASE_API_TRANSACTION = `${BASE_URL}/api/transaction`;
const BASE_API_UPLOAD = `${BASE_URL}/api/upload`;

const submitButton = $(".btn-submit");
const fields = {
    type: $("#type"),
    amount: $("#amount"),
    note: $("#note"),
    receiver: $("#receiver"),
    file: "",
};

$(function () {
    $(".receiver").hide();
    $(".top-up-file").hide();
});

fields.type.on("change", function () {
    const value = $(this).val();
    fields.file = "";
    fields.receiver = fields.receiver.val("");

    if (value === "top-up") {
        $(".receiver").hide();
        $(".top-up-file").show();
    } else if (value === "transaksi") {
        $(".receiver").show();
        $(".top-up-file").hide();
    }
});

submitButton.on("click", function () {
    console.log(fields.type.val());

    $.ajax({
        url: BASE_API_TRANSACTION,
        dataTye: "JSON",
        method: "POST",
        data: {
            type: fields.type.val(),
            amount: fields.amount.val(),
            note: fields.note.val(),
            receiver: fields.receiver.val(),
            file: fields.file,
        },
        success: (res) => {
            $(".alert").text("Transaksi berhasil di kirim");
            $(".alert").removeClass("d-none");
        },
        error: (err) => handleError(err),
    });
});

const handleError = (error) => {
    const { errors, message } = error.responseJSON;
    console.log(error);

    for (const field in fields) {
        if (errors.hasOwnProperty(field)) {
            fields[field].addClass("is-invalid").next().text(errors[field][0]);

            $("#top-up-file")
                .addClass("is-invalid")
                .next()
                .text(errors.file[0]);
        } else {
            fields[field].removeClass("is-invalid");
        }
    }
};

$(document).on("change", "#top-up-file", function () {
    const form = new FormData();
    const files = $("#top-up-file")[0].files[0];
    form.append("file", files);

    let file = $("#top-up-file").get(0).files[0];

    if (file) {
        let reader = new FileReader();

        reader.onload = function () {
            $("#top-up").removeClass("d-none").attr("src", reader.result);
        };

        reader.readAsDataURL(file);
    }

    $.ajax({
        url: BASE_API_UPLOAD,
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        data: form,
        method: "POST",
        success: (res) => {
            fields.file = res.data.file_name;
        },
        error: (err) => console.log(err),
    });
});
