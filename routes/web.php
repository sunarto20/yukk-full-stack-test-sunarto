<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('login', [AuthController::class, 'login'])->name('login');
Route::get('register', [AuthController::class, 'register'])->name('register');
Route::get('register/success', function () {
    return view('auth.success');
});
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'token'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('transaction', [TransactionController::class, 'index'])->name('transaction.index');
    Route::get('history', [HistoryController::class, 'index'])->name('history.index');
});
