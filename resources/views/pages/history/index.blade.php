@extends('pages.layout.main')

@push('scripts')
    <script src="{{ asset('assets/pages/history.js') }}"></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-12 bg-light p-3 rounded">
            <div class="d-flex justify-content-between">
                <h1 class="mb-0" id="userName"> </h1>
                <h2 class="mb-0 h4">Virtual Account : <span id="virtualAccount"></span></h2>
            </div>
            <p class="text-secondary" id="userEmail"></p>
            <div class="mt-5">
                <p class="h5">Balance</p>
                <p class="h3">Rp. <span id="balance"></span></p>
            </div>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-12 bg-light py-2 px-4 rounded d-flex justify-content-between">
            <div class="form-inline">
                <input type="text" name="search" class="form-control-sm mr-2" id="search" placeholder="Search">
                <button class="btn btn-sm btn-primary" id="search-button">Search</button>
            </div>
            <div class="form-inline">
                <select name="filter" id="filter" class="form-control-sm mr-2">
                    <option selected disabled value="">Filter Type</option>
                    <option value="top-up">Top-Up</option>
                    <option value="transaksi">Transaksi</option>
                </select>
                <button class="btn btn-sm btn-primary " id="reset-filter">Reset</button>
            </div>
        </div>
    </div>
    <div id="historyCard"></div>
    <div class="d-flex justify-content-center ">
        <div class="btn btn-primary" id="loadMore">Load More</div>
    </div>
@endsection
