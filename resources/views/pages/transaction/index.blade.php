@extends('pages.layout.main')

@push('scripts')
    <script src="{{ asset('assets/pages/transaction.js') }}"></script>
@endpush


@section('content')
    <div class="alert alert-success d-none" role="alert"></div>

    <form class="col-6" enctype="multipart/form-data">
        <div class="form-group">
            <label for="type">Jenis Transaksi</label>
            <div class="form-group">
                <select class="form-control" name="type" id="type">
                    <option selected disabled value="">Select Type</option>
                    <option @selected(old('type') == 'top-up') value="top-up">Top Up</option>
                    <option @selected(old('type') == 'transaksi') value="transaksi">Transaksi</option>
                </select>
                <p class="invalid-feedback"></p>


            </div>
        </div>

        <div class="form-group">
            <label for="amount">Amount</label>
            <input type="number" class="form-control " id="amount" placeholder="Amount" name="amount">
            <p class="invalid-feedback"></p>

        </div>

        <div class="form-group">
            <label for="note">Keterangan</label>
            <textarea name="note" id="note" cols="30" rows="10" class="form-control"></textarea>
            <p class="invalid-feedback"></p>

        </div>

        <div class="form-group receiver">
            <label for="note">Tujuan</label>
            <select class="form-control " name="receiver" id="receiver">
                <option selected disabled value="">Select Receiver</option>
                @foreach ($receivers as $receiver)
                    <option value="{{ $receiver->id }}">{{ $receiver->name }} -
                        {{ $receiver->wallet->virtual_account ?? '' }}
                    </option>
                @endforeach
            </select>
            <p class="invalid-feedback"></p>

        </div>


        <div class="top-up-file">
            <section>Bukti Top Up</section>
            <input class="form-control mb-3" name="file" type="file" id="top-up-file">
            <p class="invalid-feedback"></p>
            <img id="top-up" alt="Bukti Top Up" class="d-none w-100 h-100">
        </div>

        <div class="mb-3">
        </div>

        <button type="button" class="btn btn-primary btn-submit mt-3">Submit</button>
    </form>
@endsection
