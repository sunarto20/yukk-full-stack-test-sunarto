@include('pages.layout.header')

<nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="{{ url('/') }}">Home</a>
                <a class="nav-item nav-link" href="{{ route('transaction.index') }}">Transaksi</a>
                <a class="nav-item nav-link" href="{{ route('history.index') }}">History</a>
                <a class="nav-item nav-link" href="{{ route('logout') }}">Logout</a>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    @yield('content')
</div>


@include('pages.layout.footer')
