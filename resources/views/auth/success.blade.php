@extends('auth.layout.main')

@push('scripts')
@endpush

@section('content')
    <div class="">

        <h1>User has been success for registered, Please Login</h1>
        <a href="{{ route('login') }}" class="btn btn-primary">Log In</a>
    </div>
@endsection
