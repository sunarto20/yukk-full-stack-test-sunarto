@extends('auth.layout.main')

@push('scripts')
    <script src="{{ asset('assets/auth/register.js') }}"></script>
@endpush

@section('content')
    <form class="form-signin">
        <h1 class="h3 mb-3 font-weight-normal">Register User</h1>
        <form>
            <div class="mb-3">
                <label for="name" class="sr-only">Name</label>
                <input type="text" id="name" name="name" class="form-control " placeholder="Fullname" autofocus>
                <p class="text-left invalid-feedback"></p>
            </div>
            <div class="mb-3">
                <label for="email" class="sr-only">Email</label>
                <input type="text" id="email" name="email" class="form-control " placeholder="Email" autofocus>
                <p class="text-left invalid-feedback"></p>
            </div>
            <div class="mb-3">
                <label for="username" class="sr-only">Username</label>
                <input type="text" id="username" name="username" class="form-control " placeholder="Username" autofocus>
                <p class="text-left invalid-feedback"></p>
            </div>
            <div class="">
                <label for="password" class="sr-only">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <p class="text-left invalid-feedback"></p>

            </div>
            <div class="">
                <button class="btn btn-lg btn-primary btn-block mb-1" id="btn-submit" type="submit">Register</button>
                <p>Sudah punya akun? <br>silahkan <a href="{{ route('login') }}"> Masuk</a></p>
            </div>
        </form>
    </form>
@endsection
