@extends('auth.layout.main')

@push('scripts')
    <script src="{{ asset('assets/auth/login.js') }}"></script>
@endpush

@section('content')
    <form class="form-signin">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        <div class="alert alert-danger" role="alert"></div>

        <form>
            <div class="mb-3">
                <label for="username" class="sr-only">Email or Username</label>
                <input type="text" id="username" name="username" class="form-control " placeholder="Email or Username"
                    autofocus>
                <p class="text-left invalid-feedback"></p>
            </div>
            <div class="">
                <label for="password" class="sr-only">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <p class="text-left invalid-feedback"></p>

            </div>
            <div class="">
                <button class="btn btn-lg btn-primary btn-block mb-1" id="btn-submit" type="submit">Sign in</button>
                <p>Belum punya akun? <br>silahkan <a href="{{ route('register') }}"> daftar disini</a></p>
            </div>
        </form>
    </form>
@endsection
